module.exports = function(web, app) {

    web.route('/map')
        .get(function(req, res) {
            res.render('index', {
                title: 'DutchOpenHackathon',
                
                content: 'partials/gmaps.html'
           });
        });

};
