module.exports = function(web, app) {

    web.route('/')
        .get(function(req, res) {
            res.render('index', {
                title: 'DutchOpenHackathon',
           });
        });

};
