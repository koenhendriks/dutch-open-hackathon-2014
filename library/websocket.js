module.exports = function() {
    var _socket     = require('socket.io'),
        _connection = null,
        _clients    = {};

    /**
     * Start the websocket server
     */
    function listen(port, callback) {
        _connection = _socket.listen(port).set('log level', 0);

        _connection.on('connection', function(socket) {
            
            _clients.push({
                id: socket.id,
                sock: socket
            });

        });

        callback();
    };

    /**
     * Event listener
     */
    function on(eventName, callback) {
        _connection.on(eventName, function(data) {

        });
    };

    /**
     * Emit data to a specified client, if the client
     * value is -1, it will send the data to all connected
     * clients.
     */
    function send(client, eventName, data) {

    };

    function getClients() {

    };

    return {
        listen: listen,
        on: on,
        send: send
    }
};