/**
 * Created by koen on 20-9-14.
 */
var lineColors = ["#FF0000", "#0000FF"];
var map;
var markers = [];
var marker_key = 0;
var marker = '';
var vehicleHistories = [];
var historyPaths = [];

//Wait for document (with elements) are loaded
$(document).ready(function(){
    google.maps.event.addDomListener(window, 'load', initialize);
});



/**
 * Initialize google map
 */
function initialize() {
    var mapOptions = {
        center: {lat: 52.30690754351726, lng: 4.766875505447388},
        zoom: 13
    };

    map = new google.maps.Map(document.getElementById('gmaps'),
        mapOptions);

    //Functions when clicked on the map
    google.maps.event.addListener(map,'click',function(event) {
        $('#debug-click-lng').html(event.latLng.lng());
        $('#debug-click-lat').html(event.latLng.lat());

        //addVehicle(event.latLng.lat(),event.latLng.lng());
    });

    //Functions for mousemovement on the map
    google.maps.event.addListener(map,'mousemove',function(event) {
        $('#debug-hover-lat').html(event.latLng.lat());
        $('#debug-hover-lng').html(event.latLng.lng());
    });
}

/**
 * Draw line from all the gps points in the history array.
 *
 * @param history
 */
function drawHistory(vehicleName){
    historyPaths[vehicleName] = new google.maps.Polyline({
        path: vehicleHistories[vehicleName],
        geodesic: true,
        strokeColor: '#ff0000',
        strokeOpacity: 0.3,
        strokeWeight: 3
    });

    historyPaths[vehicleName].setMap(map);
}

function hideHistory(vehicleName){
    historyPaths[vehicleName].setMap(null);
}

/**
 * Set a vehicle to the map, or update its location
 *
 * @param lat latitude of vehicle
 * @param lng longitude of vehicle
 * @param name of the vehicle. If not set an auto increasing number will be set as key
 */
function setVehicle(lat, lng, name){
    var maplocation = new google.maps.LatLng(lat,lng);

    if(markers[name] !== undefined){
        markers[name].setPosition(maplocation);
    }
    else {
        if(name === undefined || name == '' ){
            name = marker_key;
            marker_key = (marker_key+1);
        }

        markers[name] = new google.maps.Marker({
            position: maplocation,
            map:map,
            draggable:false,
            animation: google.maps.Animation.DROP,
            icon: 'truck.png'
        });
    }

}

/**
 * Remove a vehicle from the map
 *
 * @param name
 */
function removeVehicle(name){
    if(markers[name] === undefined){
        console.log('Vehicle doesn\'t exists');
    }
    else{
        markers[name].setMap(null);
    }
}