var nonActive = [];

(function() {
    'use strict';

    /**
     * Connect the websocket with the server
     * TODO: Load from dynamic host
     */
    websocket.connect('http://localhost:8080/');

    /**
     * Recieves a list with all current acitve vehicles
     */
    websocket.on('vehicle-init', function(data) {
        if(data.err) {
            return;
        }
        
        var vehicles = data.vehicles;

        // Remove the loader
        $('#loader').fadeOut();

        console.log('vehicle-init', vehicles);
        $('#info-bar-title').html(vehicles.length+' Voertuigen');

        vehicles.forEach(function(vehicle){
            var history = [];
            $('.info-bar-panels').append('' +
                '<div class="panel panel-default">' +
                    '<div class="panel-heading">' +
                        '<h4 class="panel-title">' +
                            '<a data-toggle="collapse" data-parent="#accordion" href="#'+vehicle['objectuid']+'">' +
                                vehicle['licenseplatenumber'] +
                            '</a>' +
                        '</h4>' +
                    '</div>' +
                    '<div id="'+vehicle['objectuid']+'" class="panel-collapse collapse">' +
                    '<div class="panel-body">' +
                        '<span class="vehicle-spec">Type: </span>'+
                            vehicle['vehicletype']+
                        '<br/>'+
                        '<span class="vehicle-spec">Kleur: </span>'+
                            vehicle['vehiclecolor']+
                        '<br/>'+
                        '<span class="vehicle-spec">Brandstof: </span>'+
                            vehicle['fl_fueltype']+
                        '</span><br/>'+
                        '<span class="vehicle-spec">Hoogte: </span>'+
                            (vehicle['height']/1000)+
                        ' Meter<br/>'+
                        '<span class="vehicle-spec">Lengte: </span>'+
                            (vehicle['length']/1000)+
                        ' Meter<br/>'+
                        '<span class="vehicle-spec">Breedte: </span>'+
                            (vehicle['width']/1000)+
                        ' Meter<br/>'+
                        '<span class="vehicle-spec">Uniek id: </span>'+
                            vehicle['objectuid']+
                        '<br/>'+
                        '<label>'+
                            '<input type="checkbox" id="check-history-'+vehicle['objectuid']+'" onchange="checkHistory(\''+vehicle['objectuid']+'\')" /> Geschiedenis'+
                        '</label>'+
                    '</div>' +
                    '</div>' +
                '</div>'
            );

            vehicle['track'].forEach(function(location){
                var historyLat = (location['latitude']/1000000);
                var historyLng = (location['longitude']/1000000);
                history.push(new google.maps.LatLng(historyLat,historyLng));
                vehicleHistories[vehicle['objectuid']] = history;
            });


            var vehiclePosition = vehicle['track'].slice(-1)[0];
            if(vehiclePosition === undefined){
                nonActive.push(vehicle['objectuid']);
            }
            else{
                var vehicleLat = (vehiclePosition['latitude']/1000000);
                var vehicleLng = (vehiclePosition['longitude']/1000000);

                setVehicle(vehicleLat, vehicleLng, vehicle['objectuid']);
            }


            //setVehicle(vehicle['ojectuid'], vehicleLat, vehicleLng);
        });//End vehicle loop
    });

    /**
     * Receives a new list with vehciles (added or removed)
     */
    websocket.on('vehicle-listupdate', function(data) {
        if(data.err) {
            return;
        }
    });

    /**
     * Receives a list with all vehicles and track information.
     */
    websocket.on('vehicle-trackupdate', function(data) {
        if(data.err) {
            return;
        }

        var vehicles = data.trackInfo;

        vehicles.forEach(function(vehicle){
            var vehiclePosition = vehicle['track'].slice(-1)[0];
            if(vehiclePosition === undefined){
                nonActive.push(vehicle['objectuid']);
            } else {
                var vehicleLat = (vehiclePosition['lat']/1000000),
                    vehicleLat = (vehiclePosition['lat']/1000000),
                    vehicleLng = (vehiclePosition['lng']/1000000);

                setVehicle(vehicleLat, vehicleLng, vehicle['objectuid'] );
            }
        });
    });

    // Test sockets stuff here
    //websocket.connect('http://localhost:8080/');
    //websocket.send('test', {});
    //websocket.on('hi', function(e) {
    //  console.log('SOCKET', e);
    //});


}());

// Public scope /!\
function checkHistory(vehicleName){
    //console.log($('#check-history-'+vehicleName));
    if($('#check-history-'+vehicleName).is(':checked'))
        drawHistory(vehicleName);
    else
        hideHistory(vehicleName);
}