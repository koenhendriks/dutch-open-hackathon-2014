var websocket = (function() {
    var _socket = null;

    function connect(hostname) {
        _socket = io.connect(hostname);
    }; 

    function send(eventName, data) {
        _socket.emit(eventName, data);
    };

    function on(eventName, callback) {
        _socket.on(eventName, function(data) {
            callback(data);
        });
    };

    return {
        connect: connect,
        send: send,
        on: on
    }
}());
