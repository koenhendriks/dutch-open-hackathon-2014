/**
 * Created by koen on 20-9-14.
 */
var data =
    {
        unique_bus_id_1 :     {
            license_plate : 'AA-BB-CC',
            locations: [
                {lat: 52.301682235117276, lng: 4.753968715667725},
                {lat: 52.30188561709786,lng: 4.754612445831299},
                {lat: 52.302325181413075,lng: 4.755771160125732},
                {lat: 52.302115240791956,lng: 4.7560179233551025},
                {lat: 52.301813449405245,lng: 4.756275415420532},
                {lat: 52.30159038488435,lng: 4.756554365158081},
                {lat: 52.3013541976963, lng: 4.756779670715332},
                {lat: 52.30132795459762, lng: 4.756479263305664}
            ]
        }
    };


var numDeltas = 100;
var delay = 10; //milliseconds
var i = 0;
var deltaLat;
var deltaLng;
var position = [];
var result = [];

var latlng = new google.maps.LatLng(position[0], position[1]);
var marker;

function go(markerName, toLat, toLng){

    marker = markers[markerName];

    position = [marker.getPosition().lat(), marker.getPosition().lng()];


    result = [toLat, toLng];


    transition(marker, result)
}


function transition(marker, result){
    i = 0;
    deltaLat = (result[0] - position[0])/numDeltas;
    deltaLng = (result[1] - position[1])/numDeltas;
    moveMarker();
}

function moveMarker(){
    position[0] += deltaLat;
    position[1] += deltaLng;
    var latlng = new google.maps.LatLng(position[0], position[1]);
    marker.setPosition(latlng);
    if(i!=numDeltas){
        i++;
        setTimeout(moveMarker, delay);
    }
}