module.exports = function(socket, api) {
    // Data array
    var _tmr_webfleetUpdateVehicleList      = 1000 * 60 * 5,    // 5 minutes
        _tmr_webfleetUpdateVehicleData      = 1000 * 60;        // 1 minute

    // Local data
    var _vehicles = [];

    // Webclients
    var _clients = [];

    /**
     * Startup the service provider
     */
    function startUp(callback, startServer) {

        // Update vehicles list
        (function loop() {
            _webfleetUpdateVehicleList(function(){
                setTimeout(loop, _tmr_webfleetUpdateVehicleList);
            });
        }());

        // Update vehicle positions and meta data
        (function loop() {
            _webfleetUpdateVehicleData(function() {
                _socketDeployVehicleData();
                setTimeout(loop, _tmr_webfleetUpdateVehicleData);
            });
        }());

        // Add a eventlistener for the websockets onConnect
        socket.on('connection', function(sock) {
            // Add to the client list
            _clients.push({
                auth: true, // TODO: Auth
                socket: sock
            });
            console.log('New websocket connection (connections: %d)', _clients.length);

            // Wait before the service provider is ready.
            (function check() {
                if(_vehicles.length == 0) {
                    console.log('Waiting for the vehicle tracks');
                    setTimeout(check, 2000);
                    return;
                }

                for(var i = 0; i < _vehicles.length; i++) {
                    if(_vehicles[i].track == undefined) {
                        console.log('Waiting for the vehicle tracks');
                        setTimeout(check, 2000);
                        return;
                    }
                }

                _onSocketConnectReady(sock);
            }());

            // Add disconnect even to cleanup the client list
            sock.on('disconnect', function() {
                for(var i = 0; i < _clients.length; i++) {
                    if(_clients[i].socket.id == sock.id) {
                        _clients.splice(i, 1);
                    }
                }
                console.log('Closed websocket connection (connections: %d)', _clients.length);
            });
        });

        callback();
    };

    /**
     * When a client connects, send all the current data
     */
    function _onSocketConnectReady(sock) {
        // Send the current vehicle list
        sock.emit('vehicle-init', {
            err: false,
            vehicles: _vehicles
        });
    };

    /**
     * Send vehicle data to client
     */
    function _socketDeployVehicleData(){
        var vehicleTrackInfo = [],
            trackData = [];

        for(var i = 0; i < _vehicles.length; i++) {
            if(_vehicles[i].track !== undefined) {
                trackData = [];
                for(var j = 0; j < _vehicles[i].track.length; j++) {
                    trackData.push({
                        lat: _vehicles[i].track[j].latitude,
                        lng: _vehicles[i].track[j].longitude
                    });
                }

                vehicleTrackInfo.push({
                    objectuid: _vehicles[i].objectuid,
                    track: trackData
                });
            }
        }

        for(var i  = 0; i < _clients.length; i++) {
            if(_clients[i].auth == true) {
                _clients[i].socket.emit('vehicle-trackupdate', {
                    err: false,
                    trackInfo: vehicleTrackInfo
                });
            }
        }
    };

    /**
     * Get all vehicle data
     */
    function _webfleetUpdateVehicleData(callback) {
        var updates = 0;

        if(_vehicles.length == 0) {
            callback();
        } else {
            for(var i = 0; i < _vehicles.length; i++) {
                (function(i) {
                    api.webfleet.request('showTracks', {
                        objectuid: _vehicles[i].objectuid,
                        objectno: _vehicles[i].objectno,
                        range_pattern: 'd0'
                    }, function(err, data) {
                        updates++;
                        _vehicles[i].track = data;
                        if(updates == _vehicles.length) {
                            callback();
                        }
                    });
                }(i));
            }
        }
    };

    /**
     * This function should be called every 10 minutes
     * to keep track on new or removed vehicles on the list
     */
    function _webfleetUpdateVehicleList(callback) {
        api.webfleet.request('showVehicleReportExtern', {
            range_pattern: 'd0' // Timespan in history, 0d is current
        }, function(err, data) {

            if(!err) {
                var found, add = [], del = [];

                // Scan for missing objects based on objectuid and push
                // them to the add array if missing.
                for(var i = 0; i < data.length; i++) {
                    found = false;
                    for(var j = 0; j < _vehicles.length; j++) {
                        if(data[i].objectuid === _vehicles[j].objectuid) {
                            found = true;
                        }
                    }
                    if(found === false) {
                        add.push(data[i]);
                    }
                }

                // Scan for missing vehicles in the list based on the
                // objectuid and add them to the del array if not found.
                for(var i = 0; i < _vehicles.length; i++) {
                    found = false;
                    for(var j = 0; j < data.length; j++) {
                        if(_vehicles[i].objectuid === data[j].objectuid) {
                            found = true;
                        }
                    }
                    if(found === false) {
                        del.push(i);
                    }
                }

                // Remove deleted data
                for(var i = 0; i < del.length; i++) {
                    _vehicles.splice(del[i], 1);
                }

                // Add new vehicles
                for(var i = 0; i < add.length; i++) {
                    _vehicles.push(add[i]);
                }

                if(add.length > 0 || del.length > 0) {
                    console.log('Vehicle ammount changed to %d', _vehicles.length);
                }

                callback();
            }

        });
    };

    function _webfleet() {


    };

    return {
        startUp: startUp
    }
};
