var fs          = require('fs'),
    path        = require('path'),
    swig        = require('swig'),
    express     = require('express'),
    http        = require('http'),
    socketio    = require('socket.io');

var config      = require('./config.json'),
    services    = require('./service/service.js');

var webApp      = express(),
    webServer   = http.Server(webApp),
    socket      = socketio(webServer);

var apiWebfleet = require(path.join(__dirname, 'api', 'webfleet', 'webfleet.js'))();

// Setup the service provider
services = services(socket, {
    webfleet: apiWebfleet
});

webApp.set('views', path.join(__dirname, 'views'));
webApp.set('view engine', 'html');
webApp.engine('html', swig.renderFile);
webApp.use(express.static(path.join(__dirname, 'public')));

webApp.use(function(req, res, next) {
    res.locals = {
        'baseurl':      'http://localhost:8080/'
    };

    next();
});

fs.readdirSync(path.join(__dirname, 'controllers')).forEach(function(file) {
    if(file.substr(-3) === '.js') {
        require(path.join(__dirname, 'controllers', file))(webApp, {

        });

        console.log('Loaded controller file %s', file);
    }
});

apiWebfleet.setCredentials(
    config.api.webfleet.account,
    config.api.webfleet.username,
    config.api.webfleet.password,
    config.api.webfleet.apikey);

/*apiWebfleet.request('showTracks', {
    objectno:       3001,
    objectuid:      '1-42797-9592916F2',
    range_pattern:  'd0'
}, function(data) {
    console.log(data);
});*/

// Testing socket
/*
socket.on('connection', function(sck) {
    sck.on('test', function(data) {
        sck.emit('hi', {});
    });
});

setInterval(function() {
    socket.broadcast.emit('mapupdate', {
        'vehicles': [
            {
                id: 'kevin',
                pos: [0.1, 0.1]
            },
            {
                id: 'timo',
                pos: [0.2, 0.2]
            },
            {
                id: 'koen',
                pos: [0.3, 0.3]
            }
        ]
    });
}, 500);*/

webServer.listen(8080, function() {
    console.log('Webserver started on port %d', webServer.address().port);
});

services.startUp(function() {
    console.log('Services provider started');
});
