module.exports = function() {
    var http        = require('http'),
        credentials = {
            account:    '',
            username:   '',
            password:   '',
            apikey:     ''
        };

    function setCredentials(account, username, password, apikey) {
        credentials.account = account;
        credentials.username = username;
        credentials.password = password;
        credentials.apikey = apikey;
    };

    function request(action, actionExtra, callback) {
        var path = '/extern' +
            '?account=' + credentials.account +
            '&username=' + credentials.username +
            '&password=' + credentials.password +
            '&apikey=' + credentials.apikey + 
            '&action=' + action +
            '&lang=en' +
            '&outputformat=json';

        for(var key in actionExtra) {
            path += '&' + key + '=' + actionExtra[key];
        }

        http.get({
            host:   'csv.business.tomtom.com',
            port:   80,
            path:   path,
            //host:   'localhost',
            //port:   '8080',
            //path:   '/example-api/vehicles.json',
            method: 'GET'
        }, function(res) {
            var buffer = '';
            res.setEncoding('utf8');
            res.on('data', function(chunk) {
                buffer += chunk;
            });
            res.on('end', function() {
                buffer = JSON.parse(buffer);
                callback(false, buffer);
            });
        }).on('error', function(e) {
            console.log('HTTP Error: ', e.message);
        });
    };

    return {
        setCredentials: setCredentials,
        request: request
    };
};
